#pragma once

#include "config.h"
#include "BMP085.h"

enum PrintOptions {
  PRINT_NONE,
  PRINT_READABLE,
  PRINT_PLOTTABLE,
};

enum PressureSensorModes {
  P_ULTRALOWPOWER = BMP085_ULTRALOWPOWER,
  P_STANDARD      = BMP085_STANDARD,
  P_HIGHRES       = BMP085_HIGHRES,
  P_ULTRAHIGHRES  = BMP085_ULTRAHIGHRES,
};

void blinkLed(long duration_ms = 500L);
void initSensors(BMP085 & pSens0, BMP085 & pSens1, PressureSensorModes mode);
void readPressureSensors(BMP085 & pSens0, BMP085 & pSens1, int * p0_Pa, int * p1_Pa);
void printValues(int p0_Pa, int p1_Pa, int a_Deg, PrintOptions opts = PRINT_READABLE);
void initAdc();
void initDacPwm();
void setDacPwmValues(int p0_Pa, int p1_Pa, int a_Deg);
void initLedStrip();
void setLedStripColor(int color);
void refreshLedStrip();
void initServo();
int updateServoDeg();
