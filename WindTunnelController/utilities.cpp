// Code written for a Teensy 3.2
// F_CPU at 96MHz

#include "utilities.h"
#include <WS2812Serial.h>
#include <PWMServo.h>

void blinkLed(long duration_ms) {
  static bool pinInit = false;
  if(!pinInit) {
    pinMode(BLINK_PIN, OUTPUT);
    pinInit = true;
  }
  digitalWrite(BLINK_PIN, HIGH);
  delay(duration_ms);
  digitalWrite(BLINK_PIN, LOW);
  delay(duration_ms);
}

void initSensors(BMP085 & pSens0, BMP085 & pSens1, PressureSensorModes mode) {
  // Initialise the sensor
  if(!pSens0.begin(mode)) {
    // There was a problem detecting the BMP085 ... check your connections
    while(true) {
      blinkLed(BLINK_MS_PSENS0);
    }
  }
  // Initialise the sensor
  if(!pSens1.begin(mode)) {
    // There was a problem detecting the BMP085 ... check your connections
    while(true) {
      blinkLed(BLINK_MS_PSENS1);
    }
  }
}

void readPressureSensors(BMP085 & pSens0, BMP085 & pSens1, int * p0_Pa, int * p1_Pa) {
  // Get a new sensor event
  int pSensPa0 = pSens0.readPressure();
  int pSensPa1 = pSens1.readPressure() + PSENS_PA_CAL;
  pSensPa0 = constrain(pSensPa0, PSENS_PA_MIN, PSENS_PA_MAX);
  pSensPa1 = constrain(pSensPa1, PSENS_PA_MIN, PSENS_PA_MAX);
  *p0_Pa = pSensPa0;
  *p1_Pa = pSensPa1;
}

void printValues(int p0_Pa, int p1_Pa, int a_mDeg, PrintOptions opts) {
  static bool serialInit = false;
  if(!serialInit) {
    static long tSerialInit = millis();
    Serial.begin(SERIAL_SPEED);
    // Wait for USB serial to be ready or else timeout and continue.
    while(!Serial && !((millis() - tSerialInit) > SERIAL_TIMEOUT));
    if(PRINT_PLOTTABLE == opts) {
      Serial.println("p0_Pa\tp1_Pa\ta_mDeg");
    }
    serialInit = true;
  }
  if(PRINT_READABLE == opts) {
    // Display atmospheric pressure in Pa
    Serial.print("Pressure: ");
    Serial.print(p0_Pa);
    Serial.print("Pa, ");
    Serial.print(p1_Pa);
    Serial.print("Pa, ");
    Serial.print(a_mDeg);
    Serial.println("mDeg");
  } else if(PRINT_PLOTTABLE == opts) {
    Serial.print(p0_Pa);
    Serial.print('\t');
    Serial.print(p1_Pa);
    Serial.print('\t');
    Serial.println(a_mDeg);
  }
}

void initAdc() {
  analogReadResolution(ADC_RES);
}

void initDacPwm() {
  // See also: https://www.pjrc.com/teensy/td_pulse.html
  analogWriteResolution(DAC_PWM_RES);
  analogWriteFrequency(DAC_PIN_PWM0, DAC_PWM_FREQ);
  analogWriteFrequency(DAC_PIN_PWM1, DAC_PWM_FREQ);
  analogWriteFrequency(DAC_PIN_PWM2, DAC_PWM_FREQ);
  analogWriteFrequency(DAC_PIN_PWM3, DAC_PWM_FREQ);
}

void setDacPwmValues(int p0_Pa, int p1_Pa, int a_mDeg) {
  static const int PWM_MAX = pow(2, DAC_PWM_RES);
  static const int PK = (PSENS_PA_MAX - PSENS_PA_MIN) / PWM_MAX;
  int pwm0 = (p0_Pa - PSENS_PA_MIN) / PK;
  int pwm1 = (p1_Pa - PSENS_PA_MIN) / PK;
  int pwm2 = map(a_mDeg, SERVO_A_MIN_DEG*1000, SERVO_A_MAX_DEG*1000, 0, PWM_MAX); 
  analogWrite(DAC_PIN_PWM0, pwm0);
  analogWrite(DAC_PIN_PWM1, pwm1);
  analogWrite(DAC_PIN_PWM2, pwm2);
}

static byte _drawingMemory[WS2812_LEDS * 3];         //  3 bytes per LED
DMAMEM static byte _displayMemory[WS2812_LEDS * 12]; // 12 bytes per LED

static WS2812Serial _leds{
  WS2812_LEDS, _displayMemory, _drawingMemory, WS2812_PIN, WS2812_GRB
};

void initLedStrip() {
  _leds.begin();
}

void setLedStripColor(int color) {
  for(int i=0; i < _leds.numPixels(); i++) {
    _leds.setPixel(i, color);
  }
}

void refreshLedStrip() {
  _leds.show();
}

static PWMServo _wingServo;

void initServo() {
  _wingServo.attach(SERVO_PIN);
}

int updateServoDeg() {
  static const int ADC_MAX = pow(2, ADC_RES);
  static int sp, mDeg;
  sp = analogRead(SERVO_POT_PIN);
  mDeg = map(sp, 0, ADC_MAX, SERVO_A_MIN_DEG*1000, SERVO_A_MAX_DEG*1000);
  _wingServo.write(mDeg / 1000);
  return mDeg;
}
