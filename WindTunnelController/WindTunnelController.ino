#include "utilities.h"

BMP085 pSens0{ &Wire };
BMP085 pSens1{ &Wire1 };

void setup() {
  initSensors(pSens0, pSens1, P_ULTRAHIGHRES);
  initAdc();
  initDacPwm();
  initLedStrip();
  setLedStripColor(WS2812_DIM_WHITE);
  initServo();
}

void loop() {
  static int p0_Pa;
  static int p1_Pa;
  static int a_mDeg;
  readPressureSensors(pSens0, pSens1, &p0_Pa, &p1_Pa);
  a_mDeg = updateServoDeg();
  setDacPwmValues(p0_Pa, p1_Pa, a_mDeg);
  refreshLedStrip();
  // Display the results (barometric pressure is measure in Pa)
  printValues(p0_Pa, p1_Pa, a_mDeg, PRINT_PLOTTABLE);
}
