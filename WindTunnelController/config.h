#pragma once

// Code written for Teensy 3.2 board @F_CPU 96MHz.
// Sanity check for configuration below.
#if F_CPU != 96000000L
#error Using wrong CPU speed. Set to: "96 MHz (overclock)"
#endif

#include <Arduino.h>

enum Config {
  // Config for analog output with RC filter.
  //   ___        ___
  // -|___|--o---|___|--o-------
  //   1.5k  | +  15k   |         Vout = 0V-3.3V @12bit
  //        ###        ---        fc   = 64Hz 
  //        --- 1uF    --- 100nF  fpwm = 11.718kHz
  //   GND   |          |         Gpwm = -81dB
  // --------o----------o-------
  // See also: https://www.pjrc.com/teensy/td_pulse.html
  DAC_PWM_RES  = 12,    // bit, 0..4096 for pressure values
  DAC_PWM_FREQ = 11718, // Hz @F_CPU 96MHz
  DAC_PIN_PWM0 = 22,    // PWM uses timer FTM0 (pSens0)
  DAC_PIN_PWM1 = 23,    // PWM uses timer FTM0 (pSens1)
  DAC_PIN_PWM2 = 20,    // PWM uses timer FTM0 (aPot0)
  DAC_PIN_PWM3 = 21,    // PWM uses timer FTM0 (vPot1)
  // Config for addressable 5V RGB LEDs (NEOPIXELS).
  // Uses up to 60mA for each chip.
  // 5 lights equals to up to 300mA power consumption.
  // Library WS2812Serial for interrupt free handling.
  // Usable pins 1, 5, 8, 10, 31 (overclock to 120 MHz for pin 8).
  WS2812_LEDS       = 5,
  WS2812_PIN        = 1, // TX1 uses UART1 hardware
  WS2812_DIM_WHITE  = 0x101010,
  WS2812_FULL_WHITE = 0xFFFFFF,
  // Servo control attack angle of the wing.
  ADC_RES = 10, // bit, 0..1024 for analog values.
  // See also: https://www.pjrc.com/teensy/td_libs_Servo.html
  SERVO_POT_PIN   = A3,  // Potentiometer 0V-3.3V
  SERVO_PIN       = 4,   // PWMServo pins 3,4 use timer FTM1
  SERVO_A_MIN_DEG = 70,  // attack angle in degrees
  SERVO_A_MAX_DEG = 110, // attack angle in degrees
  // Pressure sensor error output.
  BLINK_PIN       = LED_BUILTIN,
  BLINK_MS_PSENS0 = 1000,
  BLINK_MS_PSENS1 = 500,
  // BMP085 pressure sensor configuration.
  PSENS_PA_MIN =  92116, // Pressure in Pa
  PSENS_PA_MAX = 108500, // Pressure in Pa
  PSENS_PA_CAL =   -205, // Calibration between both sensors in Pa
  // Debug output
  SERIAL_SPEED = 115200, // Baud
  SERIAL_TIMEOUT = 3000, // ms
};
