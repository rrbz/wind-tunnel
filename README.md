# wind-tunnel

Source code for the wind tunnel controller for testing aerodynamics of a wing

# Pressure sensor

The sensors used in this project are [Bosch BMP085](extras/BMP085.pdf) both one for the upper part and the lower part of a wing under test.

Both sensors must be attached to a different I2C interface because the sensor address is not changable.

The sensor board uses a Adafruit compatible layout with *Vin > 3.3V* with a local voltage regulator and 3.3V IO voltage levels.

See also wiring diagram:

![](extras/IM130710002_1.jpg)
![](extras/GY-65-SCH.jpg)
